from typing import Any, Callable, Optional, Type

import numpy as np

from sdr_wrapper import Gain, SdrDevice


class NotEnoughRFPower(Exception):
    pass


def find_optimal_gain(
    sdr_class: Type[SdrDevice],
    sample_rate_hz: float = 2.048e6,
    centre_freq_hz: float = 1420.4e6,
    callback: Optional[Callable[[Gain], Any]] = None,
) -> float:
    """
    Find the optimal gain setting for the software-defined radio (SDR) device.

    Parameters
    ----------
    sdr_class: Type
        Class of SDR device to use to record data. Either `RtlSdr` or
        `MockRtlSdr`.
    sample_rate_hz : float, optional
        The sample rate of the SDR device in Hertz.
    centre_freq_hz : float, optional
        The center frequency of the spectrum in Hertz.
    callback : func, optional
        An optional function to call every time a new gain value is tried.
        The function must take a single argument: a gain value.
        In practice, this is used in the GUI to print a status message
        with the gain value being currently tried.

    Returns
    -------
    float
        The optimal gain setting.

    Raises
    ------
    NotEnoughRFPower
        If no reasonable gain setting provides enough RF power.

    Example
    -------
    >>> optimal_gain = find_optimal_gain(sample_rate_hz=2.048e6, centre_freq_hz=1420.4e6)
    """
    sdr = sdr_class()
    sdr.sample_rate = sample_rate_hz
    sdr.center_freq = centre_freq_hz

    min_gain = 1.0
    max_gain = 50.0
    num_trials = 20
    # TODO: we probably want to use the list of available gains via get_gains()
    # https://pyrtlsdr.readthedocs.io/en/latest/rtlsdr.html#rtlsdr.rtlsdr.BaseRtlSdr.get_gains
    trial_gains = np.logspace(np.log10(min_gain), np.log10(max_gain), num_trials)

    for gain in trial_gains:
        if callback:
            callback(gain)
        # NOTE: The actual gain used is rounded to the nearest value supported
        # by the device.
        # See: https://pyrtlsdr.readthedocs.io/en/latest/rtlsdr.html#rtlsdr.rtlsdr.BaseRtlSdr.gain
        sdr.gain = gain
        dat = sdr.read_samples(int(sample_rate_hz))

        # What we want is a gain value such that both the real and imaginary
        # parts of the data properly cover the range [-1, +1], but not beyond
        # that. A standard deviation of about 0.25 achieves that.
        average_stddev = 0.5 * (dat.real.std() + dat.imag.std())
        print(f"SDR gain = {sdr.get_gain():.2f}, average stddev = {average_stddev:.3f}")
        if average_stddev > 0.25:
            print(f"Found optimal gain: {gain:.2f}")
            sdr.close()
            return gain

    sdr.close()
    raise NotEnoughRFPower("Not enough RF Power, check your system!")
