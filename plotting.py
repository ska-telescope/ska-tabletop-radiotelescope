import matplotlib.pyplot as plt
from numpy.typing import NDArray


def plot_power_spectrum(freq_hz: NDArray, power: NDArray) -> plt.Figure:
    """
    Plot power spectrum, returning a matplotlib Figure object which can be
    further tweaked, exported to file, etc.

    Parameters
    ----------
    freq_hz : np.ndarray
        The center frequencies of every bin in the power spectrum.
    power : np.ndarray
        The power spectrum values.

    Returns
    -------
    plt.Figure
        The matplotlib Figure object representing the plot.
    """
    fig = plt.figure(0, figsize=(10.0, 6))
    ax = fig.add_subplot(1, 1, 1)
    plot_power_spectrum_on_axes(ax, freq_hz, power)
    return fig


def plot_power_spectrum_on_axes(ax: plt.Axes, freq_hz: NDArray, power: NDArray) -> None:
    """
    Same as `plot_power_spectrum`, but draw on existing matplotlib Axes.
    """
    HI_FREQ_MHZ = 1420.40575
    SKAO_BLUE = "#070068"
    SKAO_MAGENTA = "#E50869"

    freq_mhz = freq_hz / 1.0e6
    ax.plot(freq_mhz, power, color=SKAO_BLUE)

    ymin, ymax = ax.get_ylim()
    ax.vlines(HI_FREQ_MHZ, ymin, ymax, linestyles="dashed", colors=SKAO_MAGENTA)
    ax.set_xlabel("Frequency [MHz]")
    ax.set_ylabel("Intensity [a.u.]")
    ax.set_xlim(freq_mhz.min(), freq_mhz.max())
    ax.grid("major", linestyle=":")
