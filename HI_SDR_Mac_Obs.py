#!/usr/bin/env python3
"""
@author: s.asayama 2023/02/10
"""
import argparse
import datetime
import os

import numpy as np
import rtlsdr

from plotting import plot_power_spectrum
from record_data import record_power_spectrum
from sdr_wrapper import MockRtlSdr, parse_gain


def parse_args():
    parser = argparse.ArgumentParser(
        description=(
            "Read some data with given gain value, "
            "get its power spectrum normalised by the baseline power "
            "spectrum, and plot the result"
        )
    )
    parser.add_argument(
        "--mock",
        action="store_true",
        help="Use a mock version of the SDR device which generates white noise",
    )
    parser.add_argument(
        "-g",
        "--gain",
        type=parse_gain,
        help="SDR Gain, either a real-valued number or 'auto'",
        default="auto",
    )
    return parser.parse_args()


def main():
    args = parse_args()
    gain = args.gain

    # TODO: should make sure that the gain value here is the same as the one
    # used to run HI_SDR_Mac_Load.py
    if not os.path.exists("load.csv"):
        raise FileNotFoundError(
            "Could not find 'load.csv' in the current directory. "
            "Please run HI_SDR_Mac_Load.py beforehand, to record a baseline "
            "power spectrum."
        )

    sdr_class = MockRtlSdr if args.mock else rtlsdr.RtlSdr
    freq_hz, pwr_sky = record_power_spectrum(sdr_class, gain)

    # This assumes we've run the other script before and load.csv is available
    pwr_baseline = np.genfromtxt("load.csv", delimiter=",")

    HIspectrum = pwr_sky / pwr_baseline
    HIspectrum = HIspectrum - np.min(HIspectrum)
    HIspectrum = HIspectrum / np.max(HIspectrum)

    timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
    np.savetxt(timestamp + ".csv", HIspectrum, delimiter=",", fmt="%s")

    fig = plot_power_spectrum(freq_hz, HIspectrum)
    ax = fig.gca()
    ax.set_title("Hydrogen line signal of our milkyway", fontsize=20)
    ax.set_ylim(np.min(HIspectrum), 1.01)
    fig.tight_layout()
    fig.savefig(timestamp + ".png", dpi=300)


if __name__ == "__main__":
    main()
