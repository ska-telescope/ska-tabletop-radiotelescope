![SKAO Logo](./skao_logo_bar.jpg)

# Ska Tabletop Radiotelescope

Software scripts to run the tabletop radio telescope realised during the design thinking workshop. 
More detailed instructions can be found at: https://confluence.skatelescope.org/display/SE/2023-12-08+Design+Thinking+Workshop 

Requirements include: 

* Python 3 (https://www.python.org/downloads/)

RTL-SDR v5 SDR Drivers

* pyrtlsdr https://pypi.org/project/pyrtlsdr/
* pyrtlsdr[lib] https://pypi.org/project/pyrtlsdr/
* soapyrtlsdr https://github.com/pothosware/SoapyRTLSDR
* librtlsdr https://github.com/librtlsdr/librtlsdr

Python Libraries

* numpy https://numpy.org/
* scipy https://scipy.org/
* matplotlib https://matplotlib.org/

USB Driver for your architecture

Install libusb for your architecture (arm64, intel etc)
e.g. arch -arm64 brew install libusb
Python Scripts to Read and Calibrate
