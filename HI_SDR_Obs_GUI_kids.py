import datetime
import tkinter as tk
from typing import Optional

import numpy as np
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk,
)
from matplotlib.figure import Figure
from numpy.typing import NDArray

from gain_search import find_optimal_gain
from plotting import plot_power_spectrum_on_axes
from record_data import record_power_spectrum


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.init_gui()

        # Optimal gain setting found during the preparation step
        self._optimal_gain: Optional[float] = None

        # Baseline power spectrum recorded during the preparation step
        self._baseline_power_spectrum: Optional[NDArray] = None

    def init_gui(self):
        self.master.title("matplotlib graph")

        frame = tk.Frame(self.master)
        fig = Figure(figsize=(10.0, 7.5))
        self.ax = fig.add_subplot(1, 1, 1)
        self.fig_canvas = FigureCanvasTkAgg(fig, frame)
        self.toolbar = NavigationToolbar2Tk(self.fig_canvas, frame)
        self.fig_canvas.get_tk_widget().pack(fill=tk.BOTH, expand=True)

        self.ax.text(0.0, 0.6, "Connect an RF termination to", fontsize=32)
        self.ax.text(0.2, 0.5, "the Sawbird HI", fontsize=32)
        self.ax.text(0.05, 0.4, "then execute 'Preparation'", fontsize=32)

        self.ax.axis("off")
        self.fig_canvas.draw()

        frame.pack()
        fonts = ("", 13)
        button0 = tk.Button(
            self.master,
            text="Preparation\n(Execute one time before observations)",
            command=self.prep,
            font=fonts,
        )
        button1 = tk.Button(
            self.master,
            text="Observation the Milky Wasy\n(Before each observation, press 'Clear')",
            command=self.sky_obs,
            font=fonts,
        )
        button2 = tk.Button(
            self.master, text="Clear", command=self.fig_clear, font=fonts
        )
        button3 = tk.Button(self.master, text="Exit", command=root.quit, font=fonts)

        button0.place(x=0, y=0)
        button1.place(x=375, y=0)
        button2.place(x=670, y=0)
        button3.place(x=850, y=0)

    def fig_clear(self):
        self.ax.clear()
        self.fig_canvas.draw()

    def prep(self):
        self.ax.clear()
        self.fig_canvas.draw()

        # May raise exception: NotEnoughRFPower
        self._optimal_gain = find_optimal_gain()

        # Record baseline integrated power spectrum
        # using optimal gain setting
        __, self._baseline_power_spectrum = record_power_spectrum(self._optimal_gain)

        self.ax.text(0.2, 0.6, "Optimum Gain = %d" % (self._optimal_gain,), fontsize=32)
        self.ax.text(0.0, 0.5, "You can Observe the Milky Way!", fontsize=32)
        self.ax.text(0.1, 0.4, "Remove RF termination and", fontsize=32)
        self.ax.text(0.15, 0.3, "Connect your antenna to", fontsize=32)
        self.ax.text(0.25, 0.2, "the Sawbird HI", fontsize=32)
        self.ax.axis("off")
        self.fig_canvas.draw()

    def sky_obs(self):
        self.ax.clear()

        if self._baseline_power_spectrum is None or self._optimal_gain is None:
            raise RuntimeError("Please run the preparation step first!")

        # Record data, get power spectrum
        print(f"Recording data, using optimal gain: {self._optimal_gain:.2f}")
        freq_hz, power_spectrum = record_power_spectrum(self._optimal_gain)
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")

        # Normalise by baseline power spectrum
        HIspectrum = power_spectrum / self._baseline_power_spectrum
        HIspectrum = HIspectrum - np.min(HIspectrum)
        HIspectrum = HIspectrum / np.max(HIspectrum)

        # Plot result
        plot_power_spectrum_on_axes(self.ax, freq_hz, HIspectrum)
        self.ax.set_title("Hydrogen line signal of our milkyway", fontsize=16)
        self.ax.set_ylim(np.min(HIspectrum), 1.01)
        self.fig_canvas.draw()

        # Save CSV data
        Outdata = np.zeros((2, 256))
        freq_megahertz = freq_hz / 1.0e6
        Outdata[0] = freq_megahertz
        Outdata[1] = HIspectrum
        np.savetxt(timestamp + ".csv", Outdata.T, delimiter=",", fmt="%s")

        # Save PNG
        self.fig_canvas.print_png(timestamp + ".png")


if __name__ == "__main__":
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
