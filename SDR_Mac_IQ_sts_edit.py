#!/usr/bin/env python3
"""
@author: s.asayama 2023/02/10
"""
import matplotlib.pyplot as plt
import numpy as np

from sdr_wrapper import MockRtlSdr

# TODO: use real SDR unless user asks not to
sdr = MockRtlSdr()
sdr.sample_rate = 2.048e6  # Hz
sdr.center_freq = 1420.4e6     # Hz


Glist = [1,2,3,4,5,6,9,11,14,16,17,19,21,22,25,27,29,32,34,36,37,38,40,42,43,44,45,47]

k = 0
for gain in Glist:
    sdr.gain = gain
    dat = sdr.read_samples(2.048e6)
    r_hist, r_bins = np.histogram(dat.real, range=(-1, 1),bins=256)
    i_hist, i_bins = np.histogram(dat.imag, range=(-1, 1),bins=256)
    pm1 = r_hist[0]+r_hist[-1] + i_hist[0] + i_hist[-1]
    print("gain=%d, %.1f, %.3f" % (gain,sdr.get_gain(), pm1))
    if pm1 > 300:
        break
    k += 1



fig = plt.figure(0,figsize=(8.0, 5))
ax1 = fig.add_subplot(1, 1, 1)


sdr.gain = Glist[k]
dat = sdr.read_samples(2.048e6)

r_hist, r_bins = np.histogram(dat.real, range=(-1, 1),bins=256)
i_hist, i_bins = np.histogram(dat.imag, range=(-1, 1),bins=256)

X_r = []
for i in range(1, len(r_bins)):
    X_r.append((r_bins[i-1]+r_bins[i])/2)

ax1.plot(X_r,(r_hist/np.sum(r_hist)+i_hist/np.sum(i_hist))*256, c='r', label='Optimum SDR Gain = ' + str(Glist[k]))



sdr.gain = 'auto'
dat = sdr.read_samples(2.048e6)
r_hist, r_bins = np.histogram(dat.real, range=(-1, 1),bins=256)
i_hist, i_bins = np.histogram(dat.imag, range=(-1, 1),bins=256)

X_r = []
for i in range(1, len(r_bins)):
    X_r.append((r_bins[i-1]+r_bins[i])/2)

ax1.plot(X_r,(r_hist/np.sum(r_hist)+i_hist/np.sum(i_hist))*256, c='b', label='Auto Gain')


arr = np.random.normal(loc=0.0, scale=32, size=len(dat))
r_hist, r_bins = np.histogram(arr, range=(-128, 128),bins=256)
ax1.plot(X_r,r_hist/np.sum(r_hist)*256*2, c = 'k', label = "Gauss")
ax1.legend()
ax1.set_title("Optimum RTL-SDR Gain: " + str(Glist[k]), fontsize=18)
plt.savefig("Optimum_SDR_gain.png", dpi=300)

print("\n####################")
print("Optimum RTL-SDR Gain: " + str(Glist[k]))


sdr.close()

