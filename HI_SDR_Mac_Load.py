#!/usr/bin/env python3
"""
@author: s.asayama 2023/02/10
"""
import argparse

import numpy as np
import rtlsdr

from plotting import plot_power_spectrum
from record_data import record_power_spectrum
from sdr_wrapper import MockRtlSdr, parse_gain


def parse_args():
    parser = argparse.ArgumentParser(
        description="Read some data with given gain value and plot the result"
    )
    parser.add_argument(
        "--mock",
        action="store_true",
        help="Use a mock version of the SDR device which generates white noise",
    )
    parser.add_argument(
        "-g",
        "--gain",
        type=parse_gain,
        help="SDR Gain, either a real-valued number or 'auto'",
        default="auto",
    )
    return parser.parse_args()


def main():
    args = parse_args()
    gain = args.gain
    sdr_class = MockRtlSdr if args.mock else rtlsdr.RtlSdr

    freq_hz, pwr_integrated = record_power_spectrum(sdr_class, gain)

    # Save to file to be used as baseline values by other script
    np.savetxt("load.csv", pwr_integrated, delimiter=",", fmt="%s")

    # Plot power spectrum and save to file
    fig = plot_power_spectrum(freq_hz, pwr_integrated)

    gain_str = f"{gain:.2f}" if isinstance(gain, float) else str(gain)
    fig.gca().set_title(f"Load data with gain = {gain_str}", fontsize=20)
    fig.tight_layout()
    fig.savefig(f"Load_SDR_gain_{gain_str}.png", dpi=300)


if __name__ == "__main__":
    main()
