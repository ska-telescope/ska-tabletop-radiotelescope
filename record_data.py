from typing import Type

import numpy as np
from numpy.typing import NDArray
from scipy import fftpack
import rtlsdr

from sdr_wrapper import Gain, SdrDevice


def record_power_spectrum(
    sdr_class: Type[SdrDevice],
    gain: Gain,
    sample_rate_hz: float = 2.048e6,
    centre_freq_hz: float = 1420.4e6,
    num_samples: int = 2**26,
    max_read_size: int = 2**23,
    num_integration_bins: int = 256,
) -> tuple[NDArray, NDArray]:
    """
    Record power spectrum data using a software-defined radio (SDR) device.
    Default values are adjusted to record a small band around the HI line.

    Parameters
    ----------
    sdr_class: Type
        Class of SDR device to use to record data. Either `RtlSdr` or
        `MockRtlSdr`.
    gain : float or "auto"
        The gain setting for the SDR device
    sample_rate_hz : float, optional
        The sample rate of the SDR device in Hertz. This is the effective
        bandwidth of the power spectrum around the centre
    centre_freq_hz : float, optional
        The center frequency of the recorded spectrum in Hertz.
    num_samples : int, optional
        The total number of raw data samples to record, by default 2**26.
    max_read_size: int, optional
        Maximum number of samples to be recorded in a single read. This is
        to avoid getting an rtlsdr error complaining about insufficient
        memory when doing large reads. Must divide `num_samples`.
    num_integration_bins : int, optional
        The number of bins into which to integrate the power spectrum of the
        raw data samples. Must divide `num_samples`.

    Returns
    -------
    A tuple of 2 numpy arrays:
    - freq_hz : The center frequencies of every bin in the integrated power
        spectrum.
    - pwr_integrated : The integrated power spectrum values.

    Raises
    ------
    ValueError
        If `max_read_size` does not divide `num_samples`.
        If `num_integration_bins` does not divide `num_samples`.

    Example
    -------
    >>> freq_hz, pwr_integrated = record_power_spectrum(gain=5.0)
    """
    if num_samples % max_read_size:
        raise ValueError("max_read_size must divide num_samples")

    if num_samples % num_integration_bins:
        raise ValueError("num_integration_bins must divide num_samples")

    sdr = sdr_class()
    sdr.sample_rate = sample_rate_hz
    sdr.center_freq = centre_freq_hz
    sdr.gain = gain

    # Record data in multiple reads of `max_read_size`
    num_reads = num_samples // max_read_size
    total_power = np.zeros(num_integration_bins)

    print(
        f"Recording {num_samples} samples in {num_reads} reads of "
        f"{max_read_size} samples each"
    )

    try:
        for _ in range(num_reads):
            dat = sdr.read_samples(max_read_size)

            # Compute power spectrum, then integrate it to produce a much lower
            # resolution version
            signal_spectrum = np.fft.fftshift(fftpack.fft(dat))
            pwr = np.abs(signal_spectrum / len(dat) * 2) ** 2
            pwr_integrated = pwr.reshape(num_integration_bins, -1).sum(axis=1)

            # Accumulate power spectrum
            total_power += pwr_integrated

    except rtlsdr.rtlsdr.LibUSBError:
        # LIBUSB_ERROR_NO_MEM might be raised when trying to make large reads
        raise

    finally:
        sdr.close()

    # Compute the centre frequencies of every bin in the integrated power
    # spectrum. The total bandwidth is equal to sample_rate_hz,
    # centered on centre_freq_hz
    freq_hz = fftpack.fftfreq(num_integration_bins, d=1.0 / sdr.sample_rate)
    freq_hz = np.fft.fftshift(freq_hz)
    freq_hz = freq_hz + sdr.center_freq
    return freq_hz, total_power
